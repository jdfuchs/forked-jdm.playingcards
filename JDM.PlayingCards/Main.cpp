
// Playing Cards
// Part 1: James Michalski
// Part 2: Joshua Fuchs

#include <iostream>
#include <conio.h>

using namespace std;

// enums and structs
enum Suit {Spades, Clubs, Diamonds, Hearts};
enum Rank {Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace};
struct Card { Rank Rank; Suit Suit; };

// Function prototypes
void PrintCard(Card card);
void PrintRank(Card card);
void PrintSuit(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	// Make a couple new cards for testing
	Card card1;
	card1.Rank = Nine;
	card1.Suit = Clubs;

	Card card2;
	card2.Rank = Nine;
	card2.Suit = Diamonds;

	// Test functions
	cout << "Card 1: "; PrintCard(card1);
	cout << "Card 2: "; PrintCard(card2);
	cout << "High card: "; PrintCard(HighCard(card1, card2));

	(void)_getch();
	return 0;
}

// Print the rank and suit of a card.
void PrintCard(Card card)
{
	cout << "The ";
	PrintRank(card);
	cout << " of ";
	PrintSuit(card);
	cout << "\n";
}

// Print just the rank of a single card.
void PrintRank(Card card)
{
	switch (card.Rank)
	{
		case Two: cout << "Two"; break;
		case Three: cout << "Three"; break;
		case Four: cout << "Four"; break;
		case Five: cout << "Five"; break;
		case Six: cout << "Six"; break;
		case Seven: cout << "Seven"; break;
		case Eight: cout << "Eight"; break;
		case Nine: cout << "Nine"; break;
		case Ten: cout << "Ten"; break;
		case Jack: cout << "Jack"; break;
		case Queen: cout << "Queen"; break;
		case King: cout << "King"; break;
		case Ace: cout << "Ace"; break;
	}
}

// Print just the suit of a single card.
void PrintSuit(Card card)
{
	switch (card.Suit)
	{
		case Spades: cout << "Spades"; break;
		case Clubs: cout << "Clubs"; break;
		case Diamonds: cout << "Diamonds"; break;
		case Hearts: cout << "Hearts"; break;
	}
}

// Return the higher of two cards.
// Assume that a higher number always beats a lower number.
// If numbers are tied, assume hearts > diamonds > clubs > spades.
Card HighCard(Card card1, Card card2)
{
	// Check rank first
	if (card1.Rank != card2.Rank) return (card1.Rank > card2.Rank) ? card1 : card2;
	// Next check suit. Order defined in the enum.
	else if (card1.Suit != card2.Suit) return (card1.Suit > card2.Suit) ? card1 : card2;
	// Cards are the same
	else return card1;
}
